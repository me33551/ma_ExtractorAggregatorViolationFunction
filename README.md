# ma_ExtractorAggregatorViolationFunction

After cloning the repository and installing ruby along with the required gems the file permissions need to be changed so that "serverRiddl.rb" is executable (command "chmod +x serverRiddl.rb" on Linux)

Afterwards, the server needs to be started with the command "./serverRiddl.rb start -v"