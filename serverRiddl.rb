#!/usr/bin/env ruby

require 'logger'
require 'json'
require 'jsonpath'
require 'riddl/server'

class DoHello < Riddl::Implementation #{{{
  def response
#    return Riddl::Parameter::Simple.new('hello',{:key=>'value'}.to_json)
    return Riddl::Parameter::Complex.new('hello', 'application/json', {:key=>'hello'}.to_json)
  end
end #}}}

class ExtractJsonDataWebSocket < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage extraction")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    sensors=dataObject['sensors'].nil? ? [] : dataObject['sensors']
    data=dataObject['data'].nil? ? [] : dataObject['data']
    extractedData=[];
    #pp sensors
    #pp data

    sensors.each do |sensorArrayElement|
      #pp "#{sensorArrayElement['sensor']['name']} - #{JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav']}"
      values = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav'].nil? ? [] : JsonPath.on(data, JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav'])
      timestamps = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['timestamp'].nil? ? [] : JsonPath.on(data, JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['timestamp'])
      exceptions = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['exceptions'].nil? ? [] : JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['exceptions']
      #pp values
      #pp timestamps
      #pp exceptions

      exceptions.each do |exception|
        firstIndexOfException=values.index(exception)
        while !firstIndexOfException.nil? do
          #pp "deleting #{firstIndexOfException}"
          if values.length==timestamps.length then
            timestamps.slice!(firstIndexOfException)
          end
          values.slice!(firstIndexOfException)
          firstIndexOfException=values.index(exception)
        end
      end

      timestamps.map! do |timestamp|
        #slicing each timestamp to 23 chars in order to get 'transform it to the right format' - this is not!!! a clean solution and should be changed
        #timestamp[0,23]
        timestamp[0..22]
      end

      extractedData.push({:name => sensorArrayElement['sensor']['name'], :values => values, :timestamps => timestamps})
    end
    send(extractedData.to_json)
  end

  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end

end #}}}

class ExtractJsonDataSingleWebSocket < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage extraction before data")
#    pp(dataJson)
#    pp("onmessage extraction after data")

    begin
      dataObject=JSON.parse(dataJson)
    rescue JSON::ParserError
#      pp "an error occured"
      dataObject={:sensor => nil, :data => nil}
#      pp(dataJson)
    end

#    pp("onmessage extraction after parsing")
#    pp dataObject
    sensor=dataObject['sensor'].nil? ? nil : dataObject['sensor']
    data=dataObject['data'].nil? ? [] : dataObject['data']

    #if data is string try to parse it
    if(data.is_a? String) then
      begin
        data=JSON.parse(data)
      rescue JSON::ParserError
        #pp "already array or not parsable"
#        data=[]
        data="{}"
      end
    end

    #if data is a hash assume its an empty one
=begin
    if(data.is_a? Hash) then
#      data="{}"
    end
=end

    extractedData=nil;
#    pp data
#    pp dataObject['sensor'].nil?
#    pp sensor
   
    sensorArrayElement=sensor
    if(!sensor.nil?) then
#      pp "in if"
#      pp "#{sensorArrayElement['sensor']['name']} - #{JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav']} - #{data}"
      values = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav'].nil? ? [] : JsonPath.on(data, JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav'])
      timestamps = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['timestamp'].nil? ? [] : JsonPath.on(data, JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['timestamp'])
      exceptions = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['exceptions'].nil? ? [] : JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['exceptions']
      #pp values
      #pp timestamps
      #pp exceptions
      if timestamps.empty? then
        timestamps = dataObject['timestamp'].nil? ? [] : [dataObject['timestamp']]
      end

      exceptions.each do |exception|
        firstIndexOfException=values.index(exception)
        while !firstIndexOfException.nil? do
          #pp "deleting #{firstIndexOfException}"
          if values.length==timestamps.length then
            timestamps.slice!(firstIndexOfException)
          end
          values.slice!(firstIndexOfException)
          firstIndexOfException=values.index(exception)
        end
      end

      timestamps.map! do |timestamp|
        #slicing each timestamp to 23 chars in order to get 'transform it to the right format' - this is not!!! a clean solution and should be changed
        #timestamp[0,23]
        timestamp[0..22]
      end

      extractedData = {:name => sensorArrayElement['sensor']['name'], :sensor => sensorArrayElement['sensor'], :values => values, :timestamps => timestamps}
#      pp "extractedData"
#      pp extractedData
      send(extractedData.to_json)
    end

#    send(extractedData.to_json)
  end

  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end

end #}}}

class AggregateJsonDataWebSocket < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage aggregation")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    #pp dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()
    result = {:name => dataObject['name'], :avg => (dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()).round(2), :max => dataObject['data'].drop(1).max.round(2), :min => dataObject['data'].drop(1).min.round(2), :elements => dataObject['data'].drop(1).length()}
    #pp result
    send( result.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class AggregateJsonDataWebSocketAvg < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage aggregation")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    #pp dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()

    result = {:name => dataObject['name'], :avg => (dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()).round(2), :elements => dataObject['data'].drop(1).length()}
    #pp result
    send( result.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class AggregateJsonDataWebSocketWeightedAvg < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage aggregation")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    #pp dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()

    firstTimestamp = dataObject['timestamps'][0]
    firstData = dataObject['data'][0]
    spielverderber = dataObject['timestamps'].drop(1).reduce() { |prev, element|
      break unless DateTime.strptime(element, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(prev, '%Y-%m-%dT%H:%M:%S.%L')
      element
    }
    sorted=dataObject['timestamps'].last == spielverderber
    pp sorted
    if(!sorted) then
=begin
      pp spielverderber
      pp dataObject['timestamps']
    timestamps = dataObject['timestamps']
    data = dataObject['data']
    orderedTimestamps = timestamps.drop(1).sort { |a,b|
      DateTime.strptime(a, '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b, '%Y-%m-%dT%H:%M:%S.%L')
    }
    orderedData = data.drop(1).sort_by { |x|
      orderedTimestamps.index(timestamps[data.index(x)])
    }
=end
    
    dataWithTimeStamps=[]
    dataObject['timestamps'].each_index do |index|
      dataWithTimeStamps << {:datum => dataObject['data'][index], :timestamp => dataObject['timestamps'][index]}
    end
    
    #pp dataWithTimeStamps
    orderedDataWithTimeStamps = dataWithTimeStamps.drop(1).sort { |a,b|
      DateTime.strptime(a[:timestamp], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b[:timestamp], '%Y-%m-%dT%H:%M:%S.%L')
    }
    #pp orderedDataWithTimeStamps

  dataObject['timestamps']=[firstTimestamp]
  dataObject['data']=[firstData]
  orderedDataWithTimeStamps.each do |item|
    dataObject['timestamps'] << item[:timestamp]
    dataObject['data'] << item[:datum]
  end

end


    firstTimeStamp=dataObject['timestamps'][1]
#    pp firstTimeStamp
    lastTimeStamp=dataObject['timestamps'][-1]
#    pp lastTimeStamp
   
    firstDateTime = DateTime.strptime(firstTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
    lastDateTime = DateTime.strptime(lastTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
#    pp firstDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')
#    pp lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')


    currentDateTime = firstDateTime
    elements=0
    counter=1
    sum=0
#    pp "starting while"
    while(DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')<=DateTime.strptime(lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')) do
      if((counter+1)<dataObject['timestamps'].length && DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')>=DateTime.strptime(dataObject['timestamps'][counter+1], '%Y-%m-%dT%H:%M:%S.%L'))
#      if((counter+1)<dataObject['timestamps'].length && currentDateTime.to_time>=DateTime.strptime(dataObject['timestamps'][counter+1], '%Y-%m-%dT%H:%M:%S.%L').to_time) then
        counter+=1
      end
      elements+=1
      sum+=dataObject['data'][counter]
      currentDateTime = (currentDateTime.to_time+1/1000.0).to_datetime
#       pp !(currentDateTime.to_time>lastDateTime.to_time)
#       pp (DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')<=DateTime.strptime(lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L'))
    end
#    pp sum
#    pp counter
#    pp elements
#    pp sum/elements

=begin
    dataObject['timestamps'].each_index do |timestampIndex|
      if index>0 then
        pp timestampIndex
        pp dataObject['data'][timestampIndex]
        pp dataObject['timestamps'][timestampIndex]
      end
    end
=end
    result = {:name => dataObject['name'], :weightedAvg => (sum/elements).round(2), :elements => dataObject['data'].drop(1).length()}
    #pp result
    send( result.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class AggregateJsonDataWebSocketAll < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage aggregation")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    #pp dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()

    firstTimestamp = dataObject['timestamps'][0]
    firstData = dataObject['data'][0]
    spielverderber = dataObject['timestamps'].drop(1).reduce() { |prev, element|
      break unless DateTime.strptime(element, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(prev, '%Y-%m-%dT%H:%M:%S.%L')
      element
    }
    sorted=dataObject['timestamps'].last == spielverderber
    pp sorted
    if(!sorted) then
=begin
      pp spielverderber
      pp dataObject['timestamps']
    timestamps = dataObject['timestamps']
    data = dataObject['data']
    orderedTimestamps = timestamps.drop(1).sort { |a,b|
      DateTime.strptime(a, '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b, '%Y-%m-%dT%H:%M:%S.%L')
    }
    orderedData = data.drop(1).sort_by { |x|
      orderedTimestamps.index(timestamps[data.index(x)])
    }
=end
    
    dataWithTimeStamps=[]
    dataObject['timestamps'].each_index do |index|
      dataWithTimeStamps << {:datum => dataObject['data'][index], :timestamp => dataObject['timestamps'][index]}
    end
    
    #pp dataWithTimeStamps
    orderedDataWithTimeStamps = dataWithTimeStamps.drop(1).sort { |a,b|
      DateTime.strptime(a[:timestamp], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b[:timestamp], '%Y-%m-%dT%H:%M:%S.%L')
    }
    #pp orderedDataWithTimeStamps
    
      dataObject['timestamps']=[firstTimestamp]
      dataObject['data']=[firstData]
      orderedDataWithTimeStamps.each do |item|
        dataObject['timestamps'] << item[:timestamp]
        dataObject['data'] << item[:datum]
      end
    
    end


    firstTimeStamp=dataObject['timestamps'][1]
#    pp firstTimeStamp
    lastTimeStamp=dataObject['timestamps'][-1]
#    pp lastTimeStamp
   
    firstDateTime = DateTime.strptime(firstTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
    lastDateTime = DateTime.strptime(lastTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
#    pp firstDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')
#    pp lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')


    currentDateTime = firstDateTime
    elements=0
    counter=1
    sum=0
#    pp "starting while"
    while(DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')<=DateTime.strptime(lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')) do
      if((counter+1)<dataObject['timestamps'].length && DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')>=DateTime.strptime(dataObject['timestamps'][counter+1], '%Y-%m-%dT%H:%M:%S.%L'))
#      if((counter+1)<dataObject['timestamps'].length && currentDateTime.to_time>=DateTime.strptime(dataObject['timestamps'][counter+1], '%Y-%m-%dT%H:%M:%S.%L').to_time) then
        counter+=1
      end
      elements+=1
      sum+=dataObject['data'][counter]
      currentDateTime = (currentDateTime.to_time+1/1000.0).to_datetime
#       pp !(currentDateTime.to_time>lastDateTime.to_time)
#       pp (DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')<=DateTime.strptime(lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L'))
    end
#    pp sum
#    pp counter
#    pp elements
#    pp sum/elements

=begin
    dataObject['timestamps'].each_index do |timestampIndex|
      if index>0 then
        pp timestampIndex
        pp dataObject['data'][timestampIndex]
        pp dataObject['timestamps'][timestampIndex]
      end
    end
=end
    result = {:name => dataObject['name'], :avg => (dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()).round(2), :min => dataObject['data'].drop(1).min.round(2), :max => dataObject['data'].drop(1).max.round(2), :weightedAvg => (sum/elements).round(2), :elements => dataObject['data'].drop(1).length()}
    #pp result
    send( result.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class AggregateJsonDataWebSocketTimeSlotMinMax < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage aggregation")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    #pp dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()
    aggregator=dataObject['aggregator']
#    pp aggregator

    aggregatorArg=JSON.parse(aggregator['aggregator']['aggregator_arg']).nil? ? nil : JSON.parse(aggregator['aggregator']['aggregator_arg'])
#    pp aggregatorArg

=begin
    firstTimestamp = dataObject['timestamps'][0]
    firstData = dataObject['data'][0]
    spielverderber = dataObject['timestamps'].drop(1).reduce() { |prev, element|
      break unless DateTime.strptime(element, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(prev, '%Y-%m-%dT%H:%M:%S.%L')
      element
    }
    sorted=dataObject['timestamps'].last == spielverderber
    pp sorted
    if(!sorted) then
    
    dataWithTimeStamps=[]
    dataObject['timestamps'].each_index do |index|
      dataWithTimeStamps << {:datum => dataObject['data'][index], :timestamp => dataObject['timestamps'][index]}
    end
    
    #pp dataWithTimeStamps
    orderedDataWithTimeStamps = dataWithTimeStamps.drop(1).sort { |a,b|
      DateTime.strptime(a[:timestamp], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b[:timestamp], '%Y-%m-%dT%H:%M:%S.%L')
    }
    #pp orderedDataWithTimeStamps
    
      dataObject['timestamps']=[firstTimestamp]
      dataObject['data']=[firstData]
      orderedDataWithTimeStamps.each do |item|
        dataObject['timestamps'] << item[:timestamp]
        dataObject['data'] << item[:datum]
      end
    
    end

    firstTimeStamp=dataObject['timestamps'][1]
#    pp firstTimeStamp
    lastTimeStamp=dataObject['timestamps'][-1]
#    pp lastTimeStamp
   
    firstDateTime = DateTime.strptime(firstTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
    lastDateTime = DateTime.strptime(lastTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
#    pp firstDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')
#    pp lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')
=end

    firstAndLastTimeStamp = dataObject['timestamps'].drop(1).minmax { |a,b|
      DateTime.strptime(a, '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b, '%Y-%m-%dT%H:%M:%S.%L')
    }

#    pp firstAndLastTimeStamp
    firstDateTime = DateTime.strptime(firstAndLastTimeStamp[0], '%Y-%m-%dT%H:%M:%S.%L')
    lastDateTime = DateTime.strptime(firstAndLastTimeStamp[1], '%Y-%m-%dT%H:%M:%S.%L')

    startFrom=aggregatorArg['startFrom'].nil? ? 0 : aggregatorArg['startFrom']
    endAt=aggregatorArg['endAt'].nil? ? lastDateTime.to_time-firstDateTime.to_time : aggregatorArg['endAt']
#    pp startFrom
#    pp endAt

    elements=0
    min=nil
    max=nil
    startDateTimeString = ((firstDateTime.to_time+startFrom/1000.0).to_datetime).strftime('%Y-%m-%dT%H:%M:%S.%L')
    endDateTimeString = ((firstDateTime.to_time+endAt/1000.0).to_datetime).strftime('%Y-%m-%dT%H:%M:%S.%L')
#    pp startDateTimeString
#    pp endDateTimeString

    dataObject['data'].each_index do |index|
      if(index!=0) then
        #pp "index: #{index}"
        #pp "data: #{dataObject['data'][index]}"
        #pp "timestamps: #{dataObject['timestamps'][index]}"
        
        if((DateTime.strptime(startDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') <= DateTime.strptime(dataObject['timestamps'][index], '%Y-%m-%dT%H:%M:%S.%L')) &&  (DateTime.strptime(endDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(dataObject['timestamps'][index], '%Y-%m-%dT%H:%M:%S.%L'))) then
#          pp "within"
          elements+=1
          if(max.nil? || dataObject['data'][index] > max) then
            max = dataObject['data'][index]
          end
          if(min.nil? || dataObject['data'][index] < min) then
            min = dataObject['data'][index]
          end
        end
      end
    end
#    pp elements

    result = {:name => dataObject['name'], :min => min, :max => max, :elementsTakenIntoAccount => elements, :elements => dataObject['data'].drop(1).length()}
    #pp result
    send( result.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class AggregateJsonDataWebSocketTimeSlotMinMaxAvg < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage aggregation")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    #pp dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()
    aggregator=dataObject['aggregator']
#    pp aggregator

    aggregatorArg=JSON.parse(aggregator['aggregator']['aggregator_arg']).nil? ? nil : JSON.parse(aggregator['aggregator']['aggregator_arg'])
#    pp aggregatorArg

=begin
    firstTimestamp = dataObject['timestamps'][0]
    firstData = dataObject['data'][0]
    spielverderber = dataObject['timestamps'].drop(1).reduce() { |prev, element|
      break unless DateTime.strptime(element, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(prev, '%Y-%m-%dT%H:%M:%S.%L')
      element
    }
    sorted=dataObject['timestamps'].last == spielverderber
    pp sorted
    if(!sorted) then
    
    dataWithTimeStamps=[]
    dataObject['timestamps'].each_index do |index|
      dataWithTimeStamps << {:datum => dataObject['data'][index], :timestamp => dataObject['timestamps'][index]}
    end
    
    #pp dataWithTimeStamps
    orderedDataWithTimeStamps = dataWithTimeStamps.drop(1).sort { |a,b|
      DateTime.strptime(a[:timestamp], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b[:timestamp], '%Y-%m-%dT%H:%M:%S.%L')
    }
    #pp orderedDataWithTimeStamps
    
      dataObject['timestamps']=[firstTimestamp]
      dataObject['data']=[firstData]
      orderedDataWithTimeStamps.each do |item|
        dataObject['timestamps'] << item[:timestamp]
        dataObject['data'] << item[:datum]
      end
    
    end

    firstTimeStamp=dataObject['timestamps'][1]
#    pp firstTimeStamp
    lastTimeStamp=dataObject['timestamps'][-1]
#    pp lastTimeStamp
   
    firstDateTime = DateTime.strptime(firstTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
    lastDateTime = DateTime.strptime(lastTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
#    pp firstDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')
#    pp lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')
=end

    firstAndLastTimeStamp = dataObject['timestamps'].drop(1).minmax { |a,b|
      DateTime.strptime(a, '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b, '%Y-%m-%dT%H:%M:%S.%L')
    }

#    pp firstAndLastTimeStamp
    firstDateTime = DateTime.strptime(firstAndLastTimeStamp[0], '%Y-%m-%dT%H:%M:%S.%L')
    lastDateTime = DateTime.strptime(firstAndLastTimeStamp[1], '%Y-%m-%dT%H:%M:%S.%L')

    startFrom=aggregatorArg['startFrom'].nil? ? 0 : aggregatorArg['startFrom']
    endAt=aggregatorArg['endAt'].nil? ? lastDateTime.to_time-firstDateTime.to_time : aggregatorArg['endAt']
#    pp startFrom
#    pp endAt

    elements=0
    min=nil
    max=nil
    sum=0
    startDateTimeString = ((firstDateTime.to_time+startFrom/1000.0).to_datetime).strftime('%Y-%m-%dT%H:%M:%S.%L')
    endDateTimeString = ((firstDateTime.to_time+endAt/1000.0).to_datetime).strftime('%Y-%m-%dT%H:%M:%S.%L')
#    pp startDateTimeString
#    pp endDateTimeString

    dataObject['data'].each_index do |index|
      if(index!=0) then
        #pp "index: #{index}"
        #pp "data: #{dataObject['data'][index]}"
        #pp "timestamps: #{dataObject['timestamps'][index]}"
        
        if((DateTime.strptime(startDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') <= DateTime.strptime(dataObject['timestamps'][index], '%Y-%m-%dT%H:%M:%S.%L')) &&  (DateTime.strptime(endDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(dataObject['timestamps'][index], '%Y-%m-%dT%H:%M:%S.%L'))) then
#          pp "within"
          elements+=1
          if(max.nil? || dataObject['data'][index] > max) then
            max = dataObject['data'][index]
          end
          if(min.nil? || dataObject['data'][index] < min) then
            min = dataObject['data'][index]
          end
          sum+=dataObject['data'][index]
        end
      end
    end
#    pp elements

    result = {:name => dataObject['name'], :min => min, :max => max, :avg => (elements==0 ? nil : (sum/elements).round(2)), :elementsTakenIntoAccount => elements, :elements => dataObject['data'].drop(1).length()}
    #pp result
    send( result.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class AggregateJsonDataWebSocketTimeSlotWeightedAvg < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage aggregation")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    #pp dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()
    aggregator=dataObject['aggregator']
#    pp aggregator

    aggregatorArg=JSON.parse(aggregator['aggregator']['aggregator_arg']).nil? ? nil : JSON.parse(aggregator['aggregator']['aggregator_arg'])
#    pp aggregatorArg

    firstTimestamp = dataObject['timestamps'][0]
    firstData = dataObject['data'][0]
    spielverderber = dataObject['timestamps'].drop(1).reduce() { |prev, element|
      break unless DateTime.strptime(element, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(prev, '%Y-%m-%dT%H:%M:%S.%L')
      element
    }
    sorted=dataObject['timestamps'].last == spielverderber
#    pp sorted
    if(!sorted) then
    
    dataWithTimeStamps=[]
    dataObject['timestamps'].each_index do |index|
      dataWithTimeStamps << {:datum => dataObject['data'][index], :timestamp => dataObject['timestamps'][index]}
    end
    
    #pp dataWithTimeStamps
    orderedDataWithTimeStamps = dataWithTimeStamps.drop(1).sort { |a,b|
      DateTime.strptime(a[:timestamp], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b[:timestamp], '%Y-%m-%dT%H:%M:%S.%L')
    }
    #pp orderedDataWithTimeStamps
    
      dataObject['timestamps']=[firstTimestamp]
      dataObject['data']=[firstData]
      orderedDataWithTimeStamps.each do |item|
        dataObject['timestamps'] << item[:timestamp]
        dataObject['data'] << item[:datum]
      end
    
    end

    firstTimeStamp=dataObject['timestamps'][1]
#    pp firstTimeStamp
    lastTimeStamp=dataObject['timestamps'][-1]
#    pp lastTimeStamp
   
    firstDateTime = DateTime.strptime(firstTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
    lastDateTime = DateTime.strptime(lastTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
#    pp firstDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')
#    pp lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')


    startFrom=aggregatorArg['startFrom'].nil? ? 0 : aggregatorArg['startFrom']
    endAt=aggregatorArg['endAt'].nil? ? lastDateTime.to_time-firstDateTime.to_time : aggregatorArg['endAt']
#    pp startFrom
#    pp endAt

    currentDateTime=firstDateTime
    realElements=0
    weightedElements=0
    counter=1
    min=nil
    max=nil
    weightedSum=0
    startDateTimeString = ((firstDateTime.to_time+startFrom/1000.0).to_datetime).strftime('%Y-%m-%dT%H:%M:%S.%L')
    endDateTimeString = ((firstDateTime.to_time+endAt/1000.0).to_datetime).strftime('%Y-%m-%dT%H:%M:%S.%L')
#    pp startDateTimeString
#    pp endDateTimeString

    while(DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')<=DateTime.strptime(lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')) do
      if((counter+1)<dataObject['timestamps'].length && DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')>=DateTime.strptime(dataObject['timestamps'][counter+1], '%Y-%m-%dT%H:%M:%S.%L'))
#      if((counter+1)<dataObject['timestamps'].length && currentDateTime.to_time>=DateTime.strptime(dataObject['timestamps'][counter+1], '%Y-%m-%dT%H:%M:%S.%L').to_time) then
        counter+=1
        if((DateTime.strptime(startDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') <= DateTime.strptime(dataObject['timestamps'][counter], '%Y-%m-%dT%H:%M:%S.%L')) &&  (DateTime.strptime(endDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(dataObject['timestamps'][counter], '%Y-%m-%dT%H:%M:%S.%L'))) then
          realElements+=1
        end
      end
      if((DateTime.strptime(startDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') <= DateTime.strptime(dataObject['timestamps'][counter], '%Y-%m-%dT%H:%M:%S.%L')) &&  (DateTime.strptime(endDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(dataObject['timestamps'][counter], '%Y-%m-%dT%H:%M:%S.%L'))) then
        weightedElements+=1
        if(max.nil? || dataObject['data'][counter] > max) then
          max = dataObject['data'][counter]
        end
        if(min.nil? || dataObject['data'][counter] < min) then
          min = dataObject['data'][counter]
        end
        weightedSum+=dataObject['data'][counter]
      end
      currentDateTime = (currentDateTime.to_time+1/1000.0).to_datetime
#       pp !(currentDateTime.to_time>lastDateTime.to_time)
#       pp (DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')<=DateTime.strptime(lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L'))
    end

=begin
    dataObject['data'].each_index do |index|
      if(index!=0) then
        #pp "index: #{index}"
        #pp "data: #{dataObject['data'][index]}"
        #pp "timestamps: #{dataObject['timestamps'][index]}"
        
        if((DateTime.strptime(startDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') <= DateTime.strptime(dataObject['timestamps'][index], '%Y-%m-%dT%H:%M:%S.%L')) &&  (DateTime.strptime(endDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(dataObject['timestamps'][index], '%Y-%m-%dT%H:%M:%S.%L'))) then
#          pp "within"
          elements+=1
          if(max.nil? || dataObject['data'][index] > max) then
            max = dataObject['data'][index]
          end
          if(min.nil? || dataObject['data'][index] < min) then
            min = dataObject['data'][index]
          end
          sum+=dataObject['data'][index]
        end
      end
    end
#    pp elements
=end

    result = {:name => dataObject['name'], :min => min, :max => max, :weightedAvg => (weightedElements==0 ? nil : (weightedSum/weightedElements).round(2)), :realElementsTakenIntoAccount => realElements, :fakeElementsTakenIntoAccount => weightedElements, :elements => dataObject['data'].drop(1).length()}
    #pp result
    send( result.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class AggregateJsonDataWebSocketTimeSlotAll < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage aggregation")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    #pp dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()
    aggregator=dataObject['aggregator']
#    pp aggregator

    aggregatorArg=JSON.parse(aggregator['aggregator']['aggregator_arg']).nil? ? nil : JSON.parse(aggregator['aggregator']['aggregator_arg'])
#    pp aggregatorArg

    firstTimestamp = dataObject['timestamps'][0]
    firstData = dataObject['data'][0]
    spielverderber = dataObject['timestamps'].drop(1).reduce() { |prev, element|
      break unless DateTime.strptime(element, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(prev, '%Y-%m-%dT%H:%M:%S.%L')
      element
    }
    sorted=dataObject['timestamps'].last == spielverderber
#    pp sorted
    if(!sorted) then
    
    dataWithTimeStamps=[]
    dataObject['timestamps'].each_index do |index|
      dataWithTimeStamps << {:datum => dataObject['data'][index], :timestamp => dataObject['timestamps'][index]}
    end
    
    #pp dataWithTimeStamps
    orderedDataWithTimeStamps = dataWithTimeStamps.drop(1).sort { |a,b|
      DateTime.strptime(a[:timestamp], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b[:timestamp], '%Y-%m-%dT%H:%M:%S.%L')
    }
    #pp orderedDataWithTimeStamps
    
      dataObject['timestamps']=[firstTimestamp]
      dataObject['data']=[firstData]
      orderedDataWithTimeStamps.each do |item|
        dataObject['timestamps'] << item[:timestamp]
        dataObject['data'] << item[:datum]
      end
    
    end

    firstTimeStamp=dataObject['timestamps'][1]
#    pp firstTimeStamp
    lastTimeStamp=dataObject['timestamps'][-1]
#    pp lastTimeStamp
   
    firstDateTime = DateTime.strptime(firstTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
    lastDateTime = DateTime.strptime(lastTimeStamp, '%Y-%m-%dT%H:%M:%S.%L')
#    pp firstDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')
#    pp lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L')


    startFrom=aggregatorArg['startFrom'].nil? ? 0 : aggregatorArg['startFrom']
    endAt=aggregatorArg['endAt'].nil? ? lastDateTime.to_time-firstDateTime.to_time : aggregatorArg['endAt']
#    pp startFrom
#    pp endAt

    currentDateTime=firstDateTime
    realElements=0
    weightedElements=0
    counter=1
    min=nil
    max=nil
    realSum=0
    weightedSum=0
    startDateTimeString = ((firstDateTime.to_time+startFrom/1000.0).to_datetime).strftime('%Y-%m-%dT%H:%M:%S.%L')
    endDateTimeString = ((firstDateTime.to_time+endAt/1000.0).to_datetime).strftime('%Y-%m-%dT%H:%M:%S.%L')
#    pp startDateTimeString
#    pp endDateTimeString

    if((DateTime.strptime(startDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') <= DateTime.strptime(dataObject['timestamps'][counter], '%Y-%m-%dT%H:%M:%S.%L')) &&  (DateTime.strptime(endDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(dataObject['timestamps'][counter], '%Y-%m-%dT%H:%M:%S.%L'))) then
      realElements+=1
      realSum+=dataObject['data'][counter]
    end

    while(DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')<=DateTime.strptime(lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')) do
      if((counter+1)<dataObject['timestamps'].length && DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')>=DateTime.strptime(dataObject['timestamps'][counter+1], '%Y-%m-%dT%H:%M:%S.%L'))
#      if((counter+1)<dataObject['timestamps'].length && currentDateTime.to_time>=DateTime.strptime(dataObject['timestamps'][counter+1], '%Y-%m-%dT%H:%M:%S.%L').to_time) then
        counter+=1
        if((DateTime.strptime(startDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') <= DateTime.strptime(dataObject['timestamps'][counter], '%Y-%m-%dT%H:%M:%S.%L')) &&  (DateTime.strptime(endDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(dataObject['timestamps'][counter], '%Y-%m-%dT%H:%M:%S.%L'))) then
          realElements+=1
          realSum+=dataObject['data'][counter]
        end
      end
      if((DateTime.strptime(startDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') <= DateTime.strptime(dataObject['timestamps'][counter], '%Y-%m-%dT%H:%M:%S.%L')) &&  (DateTime.strptime(endDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(dataObject['timestamps'][counter], '%Y-%m-%dT%H:%M:%S.%L'))) then
        weightedElements+=1
        if(max.nil? || dataObject['data'][counter] > max) then
          max = dataObject['data'][counter]
        end
        if(min.nil? || dataObject['data'][counter] < min) then
          min = dataObject['data'][counter]
        end
        weightedSum+=dataObject['data'][counter]
      end
      currentDateTime = (currentDateTime.to_time+1/1000.0).to_datetime
#       pp !(currentDateTime.to_time>lastDateTime.to_time)
#       pp (DateTime.strptime(currentDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L')<=DateTime.strptime(lastDateTime.strftime('%Y-%m-%dT%H:%M:%S.%L'), '%Y-%m-%dT%H:%M:%S.%L'))
    end
#    realSum+=dataObject['data'][counter]

=begin
    dataObject['data'].each_index do |index|
      if(index!=0) then
        #pp "index: #{index}"
        #pp "data: #{dataObject['data'][index]}"
        #pp "timestamps: #{dataObject['timestamps'][index]}"
        
        if((DateTime.strptime(startDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') <= DateTime.strptime(dataObject['timestamps'][index], '%Y-%m-%dT%H:%M:%S.%L')) &&  (DateTime.strptime(endDateTimeString, '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(dataObject['timestamps'][index], '%Y-%m-%dT%H:%M:%S.%L'))) then
#          pp "within"
          elements+=1
          if(max.nil? || dataObject['data'][index] > max) then
            max = dataObject['data'][index]
          end
          if(min.nil? || dataObject['data'][index] < min) then
            min = dataObject['data'][index]
          end
          sum+=dataObject['data'][index]
        end
      end
    end
#    pp elements
=end

    result = {:name => dataObject['name'], :min => min, :max => max, :avg => (realElements==0 ? nil : (realSum/realElements).round(2)), :weightedAvg => (weightedElements==0 ? nil : (weightedSum/weightedElements).round(2)), :realElementsTakenIntoAccount => realElements, :fakeElementsTakenIntoAccount => weightedElements, :elements => dataObject['data'].drop(1).length()}
    #pp result
    send( result.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class AggregateJsonDataWebSocketMax < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage aggregation")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    #pp dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()
    result = {:name => dataObject['name'], :max => dataObject['data'].drop(1).max.round(2), :elements => dataObject['data'].drop(1).length()}
    #pp result
    send( result.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class AggregateJsonDataWebSocketMin < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage aggregation")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    #pp dataObject['data'].drop(1).inject(0.0) {|sum,element| (sum+element)}/dataObject['data'].drop(1).length()
    result = {:name => dataObject['name'], :min => dataObject['data'].drop(1).min.round(2), :elements => dataObject['data'].drop(1).length()}
    #pp result
    send( result.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class ViolationOfJsonDataWebSocket < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage violation")
    #pp(dataJson)
    dataObject = JSON.parse(dataJson)
    target = JSON.parse(dataObject['target'])
    actual = JSON.parse(dataObject['actual'])
    #pp target
    #pp actual
    #send( {:name => dataObject['name'], :violationValue => (((target['avg']-actual['avg'])/target['avg']).abs).round(2)}.to_json )
    send( {:name => dataObject['name'], :violationValue => (((target['avg']-actual['avg'])).abs).round(2)}.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class ViolationOfJsonDataWebSocketAvg < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage violation")
    #pp(dataJson)
    dataObject = JSON.parse(dataJson)
    target = JSON.parse(dataObject['target'])
    actual = JSON.parse(dataObject['actual'])
    #pp target
    #pp actual
    #send( {:name => dataObject['name'], :violationValue => (((target['avg']-actual['avg'])/target['avg']).abs).round(2)}.to_json )
    send( {:name => dataObject['name'], :violationValue => (((target['avg']-actual['avg'])).abs).round(2)}.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class ViolationOfJsonDataWebSocketMax < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage violation")
    #pp(dataJson)
    dataObject = JSON.parse(dataJson)
    target = JSON.parse(dataObject['target'])
    actual = JSON.parse(dataObject['actual'])
    #pp target
    #pp actual
    #send( {:name => dataObject['name'], :violationValue => (((target['avg']-actual['avg'])/target['avg']).abs).round(2)}.to_json )
    send( {:name => dataObject['name'], :violationValue => (((target['max']-actual['max'])).abs).round(2)}.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class ViolationOfJsonDataWebSocketMin < Riddl::WebSocketImplementation #{{{
  def onmessage(dataJson)
#    pp("onmessage violation")
    #pp(dataJson)
    dataObject = JSON.parse(dataJson)
    target = JSON.parse(dataObject['target'])
    actual = JSON.parse(dataObject['actual'])
    #pp target
    #pp actual
    #send( {:name => dataObject['name'], :violationValue => (((target['avg']-actual['avg'])/target['avg']).abs).round(2)}.to_json )
    send( {:name => dataObject['name'], :violationValue => (((target['min']-actual['min'])).abs).round(2)}.to_json )
  end
  
  def open(data)
#    printf("Received: %p\n", data)
#    send "opened"
#    printf("Sent: %p\n", data)
  end

  def onclose
#    puts "Connection closed"
  end
end #}}}

class ExtractJsonData < Riddl::Implementation #{{{
  def response
    body=JSON.parse(@p[0].value)
    sensors=body.sensors
    data=body.data
    extractedData=[];
    #pp sensors
    #pp data

    sensors.each do |sensorArrayElement|
      pp "#{sensorArrayElement['sensor']['name']} - #{JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav']}"
      values = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav'].nil? ? [] : JsonPath.on(data, JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav'])
      timestamps = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['timestamp'].nil? ? [] : JsonPath.on(data, JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['timestamp'])
      exceptions = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['exceptions'].nil? ? [] : JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['exceptions']
      pp values
      pp timestamps
      pp exceptions

      exceptions.each do |exception|
        firstIndexOfException=values.index(exception)
        while !firstIndexOfException.nil? do 
          pp "deleting #{firstIndexOfException}"
          if values.length==timestamps.length then 
            timestamps.slice!(firstIndexOfException)
          end
          values.slice!(firstIndexOfException)
          firstIndexOfException=values.index(exception)
        end
      end

      timestamps.map! do |timestamp|
        #slicing each timestamp to 23 chars in order to get 'transform it to the right format' - this is not!!! a clean solution and should be changed
        #timestamp[0,23]
        timestamp[0..22]
      end

      extractedData.push({:name => sensorArrayElement['sensor']['name'], :values => values, :timestamps => timestamps})
    end
    Riddl::Parameter::Complex.new('data', 'application/json', extractedData.to_json)
  end
end #}}}

class ExtractJsonDataOld < Riddl::Implementation #{{{
  def response
    pp @p[0]
    #pp File.read(@p[0].value)
    Riddl::Parameter::Complex.new('data', 'application/json', [].to_json)
  end
end #}}}

class ExtractData < Riddl::Implementation #{{{
  def response
    sensors=JSON.parse(@p[0].value)
    data=JSON.parse(@p[1].value)
    extractedData=[];
    #pp sensors
    #pp data

    sensors.each do |sensorArrayElement|
      pp "#{sensorArrayElement['sensor']['name']} - #{JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav']}"
      values = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav'].nil? ? [] : JsonPath.on(data, JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav'])
      timestamps = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['timestamp'].nil? ? [] : JsonPath.on(data, JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['timestamp'])
      exceptions = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['exceptions'].nil? ? [] : JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['exceptions']
      pp values
      pp timestamps
      pp exceptions

      exceptions.each do |exception|
        firstIndexOfException=values.index(exception)
        while !firstIndexOfException.nil? do 
          pp "deleting #{firstIndexOfException}"
          if values.length==timestamps.length then 
            timestamps.slice!(firstIndexOfException)
          end
          values.slice!(firstIndexOfException)
          firstIndexOfException=values.index(exception)
        end
      end

      timestamps.map! do |timestamp|
        #slicing each timestamp to 23 chars in order to get 'transform it to the right format' - this is not!!! a clean solution and should be changed
        #timestamp[0,23]
        timestamp[0..22]
      end

      extractedData.push({:name => sensorArrayElement['sensor']['name'], :values => values, :timestamps => timestamps})
    end
    Riddl::Parameter::Complex.new('data', 'application/json', extractedData.to_json)
  end
end #}}}


Riddl::Server.new(File.join(__dir__,'/serverRiddl.xml'), :host => 'localhost', :port => '12345') do
  accessible_description true
  cross_site_xhr true

  on resource do
    run AggregateJsonDataWebSocket if websocket
    run DoHello if get
    on resource "extractData" do
#      run ExtractData if get 'extractDataIn'
#      run ExtractData if post 'extractDataIn'
#      run ExtractJsonData if post
      run ExtractJsonDataWebSocket if websocket
    end
    on resource "extractDataSingle" do
      run ExtractJsonDataSingleWebSocket if websocket
    end
    on resource "aggregateData" do
      run AggregateJsonDataWebSocket if websocket
    end
    on resource "aggregateDataAvg" do
      run AggregateJsonDataWebSocketAvg if websocket
    end
    on resource "aggregateDataWeightedAvg" do
      run AggregateJsonDataWebSocketWeightedAvg if websocket
    end
    on resource "aggregateDataAll" do
      run AggregateJsonDataWebSocketAll if websocket
    end
    on resource "aggregateDataMax" do
      run AggregateJsonDataWebSocketMax if websocket
    end
    on resource "aggregateDataMin" do
      run AggregateJsonDataWebSocketMin if websocket
    end
    on resource "aggregateDataTimeSlotMinMax" do
      run AggregateJsonDataWebSocketTimeSlotMinMax if websocket
    end
    on resource "aggregateDataTimeSlotMinMaxAvg" do
      run AggregateJsonDataWebSocketTimeSlotMinMaxAvg if websocket
    end
    on resource "aggregateDataTimeSlotWeightedAvg" do
      run AggregateJsonDataWebSocketTimeSlotWeightedAvg if websocket
    end
    on resource "aggregateDataTimeSlotAll" do
      run AggregateJsonDataWebSocketTimeSlotAll if websocket
    end
    on resource "violationOfData" do
      run ViolationOfJsonDataWebSocket if websocket
    end
    on resource "violationOfDataAvg" do
      run ViolationOfJsonDataWebSocketAvg if websocket
    end
    on resource "violationOfDataMax" do
      run ViolationOfJsonDataWebSocketMax if websocket
    end
    on resource "violationOfDataMin" do
      run ViolationOfJsonDataWebSocketMin if websocket
    end
  end
end.loop!
